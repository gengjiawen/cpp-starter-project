# CMake Modules

List of all modules and third party licenses that are used in this project.

`warnings.cmake`  
`FindShellcheck.cmake`  
`FindCMakeFormat.cmake`  
`FindSphinx.cmake`  
`FindCppCheck.cmake`  
 - custom implementation (cpp-starter-project)

`cotire.cmake`  
 - Website: https://github.com/sakra/cotire (2018-12-15)
 - License: [MIT License](https://github.com/sakra/cotire/blob/master/license)

 `FindASan.cmake`  
 `FindMSan.cmake`  
 `FindSanitizers.cmake`  
 `FindTSan.cmake`  
 `FindUBSan.cmake`  
 `sanitize-helpers.cmake`  
  - Website: https://github.com/arsenm/sanitizers-cmake (2018-12-15)
  - License: `The MIT License (MIT) Copyright (c) | 2013 Matthew Arsenault | 2015-2016 RWTH Aachen University, Federal Republic of Germany`

`GetGitRevisionDescription.cmake`
`GetGitRevisionDescription.cmake.in`
 - TODO

`coverage.cmake`
`Gcov.cmake`
`Findgcov.cmake`
`Findlcov.cmake`
 - Website: https://github.com/cginternals/cmake-init
 - License: [MIT License](https://github.com/cginternals/cmake-init/blob/master/LICENSE)
